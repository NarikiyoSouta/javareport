import javax.swing.*;
import java.awt.event.*;

public class JavaReport3 {
    private JPanel root;
    private JButton tempuraButton;
    private JButton udonButton;
    private JButton yakisobaButton;
    private JButton karaageButton;
    private JLabel toplabel;
    private JButton gyozaButton;
    private JButton ramenButton;
    private JLabel OrderedItem;
    private JButton checkOutButton;
    private JLabel total;
    private JTextPane ItemsList;
    private JTextPane price;
    private JTextPane receivedInfo;

    int prices = 0;
    void order(String food){
        int conformation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food +"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);


            if (conformation == 0) {
                    JOptionPane.showMessageDialog(null, "Thank you for ordering " + food + "! It will be served as soon as possible.");
                    ItemsList.setText(ItemsList.getText() + food + "\n");
                    prices += 100;
                    price.setText(prices + "yen");
            };

    }

    public JavaReport3() {
        price.setText("0 yen");
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("tempura");
            }
        });

        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("udon");
            }
        });

        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("karaage");
            }
        });

        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("yakisoba");
            }
        });

        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("gyoza");
            }
        });


        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("ramen");
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int check = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "checkout",
                        JOptionPane.YES_NO_OPTION);

                if(check == 0){
                    JOptionPane.showMessageDialog(null,
                            "Thank you. The total price " + prices +" yen");
                    ItemsList.setText("");
                    prices = 0;
                    price.setText(prices + " yen");
                }

            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("JavaReport3");
        frame.setContentPane(new JavaReport3().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
